
Discontinuity Best Practices

### Project managemnt

- Always add pdf exports of any source file where applicable
- aggiungere ogni `deliverable` (pdf) all cartella del progetto in modo da poter vedere a colpo d'occhio tutto il materiale generato
- aggiungere ogni materiale consegnato dal cliente alla cartella del progetto in modo che chiunque abbia tutto il materiale sotto mano indiipendentemnete dal fatto che abbia ricevuto un email o meno.

## Dev

### Tools

- Docker
- Yarn
- Yeoman

    $ brew install yarn
    $ yarn global add yeoman rest-generator discontinuity-generator

### Editor

- Atom to configure with sync-settings:
  - Settings URL 
